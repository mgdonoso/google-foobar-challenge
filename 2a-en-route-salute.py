def solution(s):
    """Efficiency increase proposal for Commander Lambda: remove hall salutes!

    Calculates the number of salutes that will take place in the future given
    a current hall disposition.
    
    Arguments
        s (str): a string between 1 and 100 characters made up of:
            * > an employee walking to the right.
            * < an employee walking to the left.
            * - an empty space.
    """
    assert isinstance(s, str)
    assert 0 < len(s)
    valid_chars = [">", "<", "-"]
    assert all(char in valid_chars for char in s)

    right_salutes = 0
    right_walkers = 0
    for char in filter(lambda ch: ch is not "-", s):  # Empty spaces are irrelevant.
        if char == ">":
            right_walkers += 1
        else:  # At this point it must be a "<", no need to check.
            right_salutes += right_walkers

    return 2 * right_salutes  # In every encounter two salutes take place.


assert solution(">----<") == 2
assert solution("<<>><") == 4

