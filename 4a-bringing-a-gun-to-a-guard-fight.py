def solution(dimensions, your_position, guard_position, distance):
    """Bringing a Gun to a Guard Fight.

    To solve, extend the grid in all directions, sequentially mirroring the
    the room along the wall common to both the current and previous step.

    For dimensions=[3,2], your_position=[1,1], guard_position=[2,1] that is:

      +-+-+-+          +--+--+--+--+--+--+    Y = you
      +-Y-G-+    ->    +--Y--G--+--G--Y--+    G = guard
      +-+-+-+          +--+--+--+--+--+--+

    Keep track of both the rays that hit the guard and the ones that hit
    yourself to avoid hitting yourself before the guard.

    Arguments
        dimensions (int, int) such that 1 < dimension <= 1250.
        your_position (int, int) such that 0 < your_position < dimension.
        guard_position (int, int) such that 0 < guard_position < dimension.
        distance (int) such that 1 < distance <= 10000.
    Returns
        (int) number of distinct directions that you can fire to hit the elite
        guard, given the maximum distance that the beam can travel.
    """
    from functools import reduce

    def gcd(int_a, int_b):
        """Greatest common divisor via Euclid's algorithm."""
        if int_b:
            return gcd(int_b, int_a % int_b)
        else:
            return abs(int_a)

    def to_ray(vector):
        """Express a direction in simplest form, i.e. in terms of coprimes."""
        divisor = gcd(*vector) if any(vector) else 1
        return vector[0] // divisor,  vector[1] // divisor

    def sum_vectors(*vectors):
        return list(map(sum, zip(*vectors)))

    def scale_vector(factors, vector):
        return [factor*coord for factor, coord in zip(factors, vector)]

    def distance_square(vector):
        return vector[0]**2 + vector[1]**2

    def flip_vector(vector):
        """Reflect a direction in all possible four ways:
          0 -> neutral element.
          1 -> horizontal flip.
          2 -> vertical flip.
          3 -> 180 degrees rotation.
        """
        return [
            [                vector[0],                 vector[1]],
            [dimensions[0] - vector[0],                 vector[1]],
            [                vector[0], dimensions[1] - vector[1]],
            [dimensions[0] - vector[0], dimensions[1] - vector[1]],
        ]

    def flip_index(room):
        """Calculates the reflection index of the room.

        The reflections are isomorphic to the Klein four-group, and can de
        deduced from the parities of the room grid coordinates.

        The group can be represented as bit strings under bitwise XOR:
          00 = 0 -> neutral element.
          01 = 1 -> horizontal flip.
          10 = 2 -> vertical flip.
          11 = 3 -> 180 degrees rotation.

        Note that they coincide with the indices in which the function
        flip_vector stores the corresponding reflections.
        """
        return (room[0]%2) ^ 2*(room[1]%2)

    def make_boundary(layer):
        """Points with positive coordinates whose bigger one equals layer.
        
        Assumes that the biggest room dimension is the horizontal one.
        """
        boundary = []
        if layer <= max_room[0]:
            boundary.append([layer, 0])
            row = 1
            while row < layer:
                min_dist = scale_vector(dimensions, [layer-1, row-1])
                if distance_square(min_dist) <= max_dist2:
                    boundary.append([layer, row])
                    row += 1
                    continue
                break
        boundary.append([0, layer])
        col = 1
        while col <= layer:
            min_dist = scale_vector(dimensions, [col-1, layer-1])
            if distance_square(min_dist) <= max_dist2:
                boundary.append([col, layer])
                col += 1
                continue
            break
        boundary.sort()
        return boundary

    # Simplify the input parameters to simplest terms.

    scale = reduce(gcd, [
        dimensions[0], dimensions[1],
        your_position[0], your_position[1],
        guard_position[0], guard_position[1],
        distance
    ])
    if scale > 1:
        dimensions, your_position, guard_position = list(map(
            lambda parameter: [component // scale for component in parameter], 
            [dimensions, your_position, guard_position]
        ))
        distance = distance // scale

    # If needed, rotate the input parameters so that the horizontal dimension
    # is the bigger one. Relevant for the make_boundary function.

    if dimensions[1] > dimensions[0]:
        dimensions, your_position, guard_position = list(map(
            lambda parameter: [parameter[1], parameter[0]], 
            [dimensions, your_position, guard_position]
        ))

    # Calculate the relative positions of the reflected guard and yourself.

    your_relative_positions = list(map(
        lambda flip: sum_vectors(flip, scale_vector([-1, -1], your_position)),
        flip_vector(your_position)
    ))
    guard_relative_positions = list(map(
        lambda flip: sum_vectors(flip, scale_vector([-1, -1], your_position)),
        flip_vector(guard_position)
    ))

    # Bounds

    max_dist2 = distance**2
    max_room = [distance//dim + 1 for dim in dimensions]
    max_layer = max(max_room)

    # Setup

    rays_valid = {True: set(), False: set()}
    unvisited = []

    # Solve

    layer = 0
    while layer <= max_layer:  # Iterate through the layers of the grid of rooms.
        for room in make_boundary(layer):
            origin = scale_vector(dimensions, room)  # Position of the origin of the room.
            flip = flip_index(room)  # Reflection of the room.
            for relative, is_guard in [
                (guard_relative_positions, True),
                (your_relative_positions, False),
            ]:
                # Reflect the origin to all quadrants (preserves the flip).
                positions = list(map(
                    lambda factor: sum_vectors(scale_vector(factor, origin), relative[flip]),
                    [[+1, +1], [+1, -1], [-1, +1], [-1, -1]]
                ))
                for position in positions:
                    dist2 = distance_square(position)
                    if dist2 <= max_dist2:  # Distance constraint.
                        unvisited.append((dist2, to_ray(position), is_guard))

        unvisited.sort()  # Check the closest ones first to avoid hitting yourself.
        while unvisited:
            _, ray, is_valid = unvisited.pop(0)
            if ray not in rays_valid[True] and ray not in rays_valid[False]:
                rays_valid[is_valid].add(ray)

        layer += 1

    return len(rays_valid[True])


assert solution([2,3], [1,1], [1,2], 4) == 7
assert solution([300,275], [150,150], [185,100], 500) == 9

