def solution(m):
    """Doomsday Fuel.

    Help the scientists increase fuel creation efficiency by predicting the
    end state of a given ore sample!

    To do so, we obtain the fundamental matrix of absorbing Markov chains and
    multiply it by the matrix of transition probabilities of transient states
    to terminal states.

    Arguments
        m (square array of arrays of nonnegative ints): The element at row i
        and column j represents how many times state i has gone to state j.

    Returns
        (array) An array of ints for each terminal state giving the exact
        probabilities of each terminal state, represented as the numerator
        for each state, then the denominator for all of them at the end and
        in simplest form.
    """

    def prod(factors):
        """Multiply a sequence of factors."""
        result = 1
        for factor in factors:
            result *= factor
        return result

    def simplify(*fraction):
        """Express a fraction in terms of coprimes."""
        if len(fraction) == 1:
          fraction = fraction[0]
        numerator, denominator = fraction
        if numerator == 0:
            return 0, 1
        factor = 1
        while factor < max(numerator, denominator):
            factor += 1
            if numerator % factor == 0 and denominator % factor == 0:
                numerator //= factor
                denominator //= factor
                factor -= 1
        return numerator, denominator

    def sum_fracs(*fractions):
        """Sum fractions."""
        if len(fractions) == 1:
            fractions = fractions[0]
        numerator = sum(
            num * prod(
                den for j, (_, den) in enumerate(fractions) if i != j
            ) for i, (num, _) in enumerate(fractions)
        )
        denominator = prod(den for (_, den) in fractions)
        return simplify(numerator, denominator)

    def mul_fracs(*fractions):
        """Multiply fractions."""
        if len(fractions) == 1:
            fractions = fractions[0]
        numerator = prod(num for num, _ in fractions)
        denominator = prod(den for _, den in fractions) if numerator else 1
        return simplify(numerator, denominator)

    def div_fracs(a, b):
        """Divide fractions."""
        numerator = a[0]*b[1]
        denominator = a[1]*b[0] if numerator else 1
        return simplify(numerator, denominator)

    def matrix_inverse_first_row(matrix):
        """Find the first row of the inverse of the matrix."""
        size = len(matrix)
        matrix = list(map(list, zip(*matrix)))
        matrix = [row + [(int(i==0), 1)] for i, row in enumerate(matrix)]
        eliminate = lambda x, y: sum_fracs(x[1], mul_fracs((-1, 1), y, x[0]))
        for i in range(size):
            row = [div_fracs(item, matrix[i][i]) for item in matrix[i]]
            matrix[i] = list(map(simplify, row))
            for j in range(i+1, size):
                matrix[j] = list(map(simplify, (
                    map(lambda x: eliminate(x, matrix[j][i]), zip(row, matrix[j]))
                )))
        for i in range(size-1, -1, -1):
            row = [div_fracs(item, matrix[i][i]) for item in matrix[i]]
            matrix[i] = list(map(simplify, row))
            for j in range(i-1, -1, -1):
                matrix[j] = list(map(simplify, (
                    map(lambda x: eliminate(x, matrix[j][i]), zip(row, matrix[j]))
                )))
        return [item[-1] for item in matrix]

    def vector_mul_matrix(vector, matrix):
        """Right-multiply row vector with matrix."""
        matrix = [[mul_fracs(i, j) for j in row] for i, row in zip(vector, matrix)]
        return list(map(simplify, (map(sum_fracs, zip(*matrix)))))

    def standardize(fractions):
        """Expresses a set of fractions in terms of their minimum common denominator."""
        denominators = [denominator for _, denominator in fractions]
        min_common_denominator = 1
        factor = 1
        while max(denominators) > 1:
            factor += 1
            if any(denominator % factor == 0 for denominator in denominators):
                denominators = [
                    denominator // factor
                    if denominator % factor == 0 else denominator
                    for denominator in denominators
                ]
                min_common_denominator *= factor
                factor -= 1
        return [
            numerator * (min_common_denominator // denominator)
            for numerator, denominator in fractions
        ] + [min_common_denominator]

    dim = len(m)

    is_terminal = [sum(row) == 0 for row in m]
    if is_terminal[0]:  # No need to calculate more if state 0 is terminal.
        return [1] + (sum(is_terminal)-1)*[0] + [1]

    full_matrix = [
        [simplify((num, sum(row))) if num else (0, 1) for num in row] for row in m
    ]
    to_transient = [  # Transition probabilities from transient to transient states.
        [full_matrix[i][j] for j in range(dim) if not is_terminal[j]]
        for i in range(dim) if not is_terminal[i]
    ]
    to_absorbing = [  # Transition probabilities from transient to terminal states.
        [full_matrix[i][j] for j in range(dim) if is_terminal[j]]
        for i in range(dim) if not is_terminal[i]]

    one_minus_to_transient = [
        [mul_fracs((-1, 1), item) for item in row] for row in to_transient
    ]
    for i in range(len(to_transient)):
       one_minus_to_transient[i][i] = sum_fracs((1, 1), one_minus_to_transient[i][i])

    # Since the initial state is always 0 we only need
    # the first row of the fundamental matrix
    fundamental_first_row = matrix_inverse_first_row(one_minus_to_transient)

    # The probabilities of each terminal state (starting from state 0) are then
    # obtained by multiplying the fundamental matrix with the transition
    # probabilities from transient to terminal states.
    probs_to_absorbing = vector_mul_matrix(fundamental_first_row, to_absorbing)

    return standardize(probs_to_absorbing)


assert solution([
    [0, 2, 1, 0, 0],
    [0, 0, 0, 3, 4],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0]
]) == [7, 6, 8, 21]

assert solution([
    [0, 1, 0, 0, 0, 1],
    [4, 0, 0, 3, 2, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0]
]) == [0, 3, 2, 9, 14]

