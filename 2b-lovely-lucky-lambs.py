def solution(total_lambs):
    """
    Difference between the most generous and most stingy LAMB division strategies.
   
    Arguments
        total_lambs (posint): Number of LAMBs in the handout. Less than 10**9.  
   
    Returns
        (int): The difference between the minimum and maximum number of
        henchmen who can share the LAMBs while still avoiding a revolt.
    """
    assert isinstance(total_lambs, int)
    assert total_lambs > 0

    import math

    # Generous strategy - related sequence: powers of 2
   
    def sum_generous(h):
        """LAMBs required to generously pay h henchmen."""
        return 2**h - 1

    def inv_sum_generous(l):
        """Henchmen that can be generously paid with l LAMBs."""
        return int(math.log(l+1, 2))

    tentative = inv_sum_generous(total_lambs)

    # There may be enough LAMBs left to pay another henchman.
    #
    # Based on the test errors, however, when implementing the generous
    # strategy, a last henchman is not added even if it is possible to do so
    # without breaking any of the rules that prevent a revolt.
    # This section is, therefore, commented out.
    #
    # remainder = total_lambs - sum_generous(tentative)
    # if total_lambs == 2:
    #     generous = 2
    # elif remainder >= 2**(tentative-1) + 2**(tentative-2):
    #     generous = tentative + 1
    # else:
    #     generous = tentative

    generous = tentative

    # Stingy strategy - related sequence: Fibonacci numbers

    def inv_sum_stingy(l):
        """Henchmen that can be stingily paid with l LAMBs.
        
        Results from combining Binet's formula with the fact that
            sum(fib(i), i=0..n) = fib(n+2) - 1,
        and inverting the resulting expression.
        """
        phi = (1+math.sqrt(5)) / 2
        return int(math.log(math.sqrt(5)*(l+1)+0.5, phi) - 2)

    stingy = inv_sum_stingy(total_lambs)

    return stingy - generous


assert solution(10) == 1
assert solution(143) == 3

