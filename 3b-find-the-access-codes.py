def solution(l):
    """Find the Access Codes.

    Takes a list of positive integers l and counts the number of
    "lucky triples" of (li, lj, lk) where the list indices meet the
    requirement i < j < k.

    A "lucky triple" is a tuple (x, y, z) where x divides y and
    y divides z, such as (1, 2, 4).
    """
    number_of_factors_of_ = [0 for _ in l]
    lucky_triples_counter = 0

    # Discover "lucky triples" on the fly based on the number
    # of factors of the middle number in the triple.
    for k, dividend in enumerate(l):
        for j, divisor in enumerate(l[:k]):
            if dividend % divisor == 0:
                number_of_factors_of_[k] += 1
                lucky_triples_counter += number_of_factors_of_[j]

    return lucky_triples_counter


assert solution([1, 1, 1]) == 1
assert solution([1, 2, 3, 4, 5, 6]) == 3

