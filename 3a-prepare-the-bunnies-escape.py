def solution(plan):
    """
    Commander Lambda's remodeling project... and clearly nothing else.

    Modified Dijkstra shortest path algorithm with priority queue.
    Nodes are added as they are discovered and, along with node and distance,
    whether a wall has been removed is also tracked.

    Definitely unrelated to bunnies.

    Arguments
        plan (2D array): a rectangular matrix of 0s and 1s representing the
                         floor plan of a part of the space station.

    Returns
        (int) Minimum taxi-distance from left-upper corner to right-lower
              corner achievable by removing at most 1 wall. This criteria is
              adopted strictly for economic reasons, of course.
    """
    import heapq

    rows = len(plan)
    cols = len(plan[0])
    size = rows * cols

    def neighbours(row, col):
        """Returns the 2D-indices of the neighbours of (row, col)."""  
        potential = [(row+i, col+j) for (i, j) in [(0, -1), (0, 1), (-1, 0), (1, 0)]]
        return filter(lambda x: 0 <= x[0] < rows and 0 <= x[1] < cols, potential)

    def plan_to_graph(plan):
        """Converts a floor plan given as a 2D array into two 1D arrays.
        
        Each of them is a list whose nth item is a set of neighbours of the nth
        node. One contains all neighbour walls, the other all passable neighbours.
        """
        graph = {
            is_wall: [set() for _ in range(size)] for is_wall in [True, False]
        }
        for row in range(rows):
            for col in range(cols):
                for nrow, ncol in neighbours(row, col):
                    is_wall = bool(plan[nrow][ncol])
                    # 2D-indices get mapped into 1D.
                    graph[is_wall][cols*row+col].add(cols*nrow+ncol)
        return graph

    # Convert floor plan into a graph.
    graph = plan_to_graph(plan)

    # All taxi-distances must be smaller than size+1, so we set it as infinity.
    inf = size + 1
    # Need to keep separate distance records for both cases: with and without wall removed.
    distances = {wall_removed: size*[inf] for wall_removed in [True, False]}
    # Initialize distance to source (node 0).
    distances[False][0] = 1

    # Queue with tuples of distance, node index, and whether wall is removed.
    active_nodes = []
    # Initialize queue
    heapq.heappush(active_nodes, (1, 0, False))

    # Modified Dijkstra shortest path algorithm with priority queue.
    while active_nodes:
        distance, node, wall_removed = heapq.heappop(active_nodes)

        if node == size - 1:  # The last node is the target.
            break

        candidate = distance + 1  # All distances between nodes are 1.

        # Ignore wall neighbours once a wall has been removed.
        wall_checks = [False] if wall_removed else [True, False]
        # Only update a wall-removing path if it also improves non-wall-removing paths.
        dist_checks = [True, False] if wall_removed else [False]

        for is_wall in wall_checks:
            for neighbour in graph[is_wall][node]:
                current = min(distances[check][neighbour] for check in dist_checks)
                if candidate < current:
                    try:
                        active_nodes.remove(
                            (current, neighbour, wall_removed or is_wall)
                        )
                    except ValueError:
                        pass
                    distances[wall_removed][neighbour] = candidate
                    heapq.heappush(
                        active_nodes,
                        (candidate, neighbour, wall_removed or is_wall)
                    )

    # Check both distances: wall-removing and non-wall-removing.
    return min(distances[wall_removed][-1] for wall_removed in [True, False])


assert solution([
    [0, 1, 1, 0],
    [0, 0, 0, 1],
    [1, 1, 0, 0],
    [1, 1, 1, 0],
]) == 7

assert solution([
    [0, 0, 0, 0, 0, 0],
    [1, 1, 1, 1, 1, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 1, 1, 1, 1, 1],
    [0, 1, 1, 1, 1, 1],
    [0, 0, 0, 0, 0, 0],
]) == 11

