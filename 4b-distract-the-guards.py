def solution(*banana_list):
    """Distract the Guards.

    Pairs up the guards in such a way that the maximum number of guards go
    into an infinite thumb wrestling loop!

    In each match, two guards will pair off to thumb wrestle. The guard with
    fewer bananas will bet all their bananas, and the other guard will match
    the bet. The winner will receive all of the bet bananas. Once a match
    begins, the pair of guards will continue to thumb wrestle and exchange
    bananas, until both of them have the same number of bananas.

    The problem is solved by noticing that two guards will eventually stop
    playing if and only if the sum of their bananas is an integer multiple of
    a power of two.

    Maximum cardinality matching is calculated via Edmond's blossom algorithm.

    Arguments
        banana_list (list of positive integers): The amount of bananas each
            guard starts with.

    Returns
        (integer): Fewest possible number of guards that will be left to watch
            the prisoners.

    """
    try:
        from itertools import ifilter as filter  # Python 2
    except ImportError:
        pass

    def gcd(int_a, int_b):
        """Greatest common divisor via Euclid's algorithm."""
        if int_b:
            return gcd(int_b, int_a % int_b)
        else:
            return abs(int_a)

    def wrestle_forever(bananas1, bananas2):
        """Determines whether two guards will thumb wrestle forever.

        This happens if and only if the sum of their bananas is an integer
        multiple of a power of two.

        Arguments
            banana_sum (positive integer): The sum of both guard's bananas.

        Returns
            (boolean): True if and only if the two guards will thumb wrestle
                forever, False otherwise.

        """
        divisor = gcd(bananas1, bananas2)
        banana_sum = (bananas1 + bananas2) // divisor
        # A power of two in binary representation is a one followed by zeros.
        return banana_sum & (banana_sum-1)  # Bitwise AND.

    def find_maximum_matching(graph, match):
        """The backbone of Edmond's blossom algorithm."""
        path = find_augmenting_path(graph, match)
        if path:
            augment_along(match, path)
            return find_maximum_matching(graph, match)
        else:
            for node in match:
                assert match[node] in graph[node]
            return match

    def find_augmenting_path(graph, match):
        """An augmenting path is one from a free node to another free node.

        Once no augmenting path is found, the cardinality of the matches is
        certain to be maximal.

        """
        forest = set()
        for node in graph:
            if node not in match:
                forest.add(node)
        while forest:
            root = forest.pop()
            parity = 0  # Used to determine whether a cycle is even or odd.
            tree = {root: (-1, parity)}
            queue = []
            queue.append(root)
            while queue:
                node = queue.pop(0)
                for edge in graph[node]:
                    if edge not in tree and edge in match:
                        # Expand tree. Not a blossom or a free node.
                        mate = match[edge]
                        tree[edge] = (node, 1)
                        tree[mate] = (edge, 0)
                        queue.append(mate)
                    elif edge in tree:
                        if tree[node][1] == tree[edge][1]:
                            # Odd cycle (blossom) detected, contract cycle.
                            path = blossom_recursion(graph, match, tree, node, edge)
                            return path
                        else:
                            # Even cycle detected.
                            continue
                    else:
                        # Node is free (augmenting path found).
                        tree[edge] = (node, 1)
                        path = to_path(tree, edge)
                        return path

    def to_path(tree, branch):
        """Creates a path to the root of the tree."""
        path = []
        while branch != -1:
            path.append(branch)
            branch = tree[branch][0]
        return path

    def blossom_recursion(graph, match, tree, node, edge):
        """Contracts the blossom and solves the reduced problem."""
        blossom = to_blossom(tree, node, edge)
        bnode, bgraph, bmatch = contract_graph(graph, match, blossom)
        bpath = find_augmenting_path(graph, match)
        expand_graph(graph, match, bnode, bgraph, bmatch)
        if bpath:
            path = lift_path(blossom, bnode, bpath)
            return path
        return []

    def to_blossom(tree, branch1, branch2):
        """Creates a blossom from two paths with a common ancestor."""
        blossom = []
        path1 = to_path(tree, branch1)
        path2 = to_path(tree, branch2)
        for node1 in path1:
            blossom.append(node1)
            if node1 in path2:
                break
        for node2 in path2:
            if node2 == node1:
                break
            blossom.insert(0, node2)
        return blossom

    def contract_graph(graph, match, blossom):
        """Replaces all nodes in a blossom by a single supernode."""
        blossom_graph = {}
        blossom_match = {}
        blossom_node = max(graph) + 1
        graph[blossom_node] = set()
        for node in blossom:
            edges = graph.pop(node)
            for edge in edges:
                blossom_graph.setdefault(node, set()).add(edge)
                if edge not in blossom:
                    graph[blossom_node].add(edge)
                    graph[edge].add(blossom_node)
                    graph[edge].remove(node)
            if node in match:
                mate = match.pop(node)
                blossom_match[node] = mate
                if mate not in blossom:
                    match[mate] = blossom_node
        return blossom_node, blossom_graph, blossom_match

    def expand_graph(graph, match, blossom_node, blossom_graph, blossom_match):
        """Undoes the operations done by the function contract_graph."""
        del graph[blossom_node]
        for node in blossom_graph:
            graph[node] = blossom_graph[node]
            for edge in graph[node]:
                graph.setdefault(edge, set()).add(node)
                graph[edge].discard(blossom_node)
            if node in blossom_match:
                mate = blossom_match[node]
                match[node] = mate
                if mate not in blossom_graph:
                    match[mate] = node
        return None

    def lift_path(blossom, blossom_node, blossom_path):
        """Expands a path along with the corresponding blossom."""
        if blossom_node not in blossom_path:
            return blossom_path
        path = []
        while blossom_path:
            node = blossom_path.pop(0)
            # Regular node.
            if node != blossom_node:
                path.append(node)
                continue
            # Blossom traversed.
            elif path and blossom_path:
                base = lambda node: node in graph[path[-1]]
                target = lambda node: node in graph[blossom_path[0]]
            # Blossom at end.
            elif path:
                base = lambda node: node in graph[path[-1]]
                target = lambda node: node not in match
            # Blossom at start.
            else:
                base = lambda node: node not in match
                target = lambda node: node in graph[blossom_path[0]]
            base_index = blossom.index(next(filter(base, blossom)))
            blossom = blossom[base_index:] + blossom[:base_index]
            target_index = blossom.index(next(filter(target, blossom)))
            candidate = blossom[:target_index+1]
            if not len(candidate) % 2:
                candidate = [blossom[0]] + blossom[:target_index-1: -1]
            path.extend(candidate)
        return path

    def augment_along(match, path):
        """Increases a matching based on an augmenting path."""
        while path:
            node = path.pop(0)
            mate = path.pop(0)
            match[node] = mate
            match[mate] = node
        return match

    # Process input (account for int, ints and list of ints).

    if len(banana_list) == 1:
        banana_list = banana_list[0]
        if isinstance(banana_list, int):
            return 1

    # Build the adjacency matrix of the graph.

    graph = {}
    for i, bananas1 in enumerate(banana_list):
        for j, bananas2 in enumerate(banana_list[i+1:]):
            # Adjacent nodes represent guards that will thumb wrestle forever.
            if wrestle_forever(bananas1, bananas2):
                j = i+1+j
                graph.setdefault(i, set()).add(j)
                graph.setdefault(j, set()).add(i)

    # Expand the matching greedily to skip initial iterations.

    match = {}
    for node in graph:
        if node in match:
            continue
        for edge in graph[node]:
            if edge not in match:
                match[node] = edge
                match[edge] = node
                break

    # Solve.

    find_maximum_matching(graph, match)

    return len(banana_list) - len(match)


assert solution(1, 1) == 2
assert solution([1, 7, 3, 21, 13, 19]) == 0

