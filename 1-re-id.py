def solution(i):
    """Implements Commander Lambda's Completely Foolproof Scheme.

    It first concatenates all primes less than max_size, and then returns the
    next five digits after the one in position i.
    """
    assert isinstance(i, int) and i >= 0

    primes = []
    max_size = 10**5 // 4
    is_prime = max_size * [True]
    is_prime[0:2] = 2 * [False]

    # Sieve of Eratosthenes algorithm
    current = 2
    while current**2 < max_size:
        if is_prime[current]:
            index = current**2
            while index < max_size:
                is_prime[index] = False
                index += current
        current += 1

    # List the prime numbers
    for candidate, boolean in enumerate(is_prime):
        if boolean:
            primes.append(candidate)

    # Concatenate the prime numbers
    concat_primes = "".join(map(str, primes))

    return concat_primes[i:i+5]


assert solution(0) == "23571"
assert solution(3) == "71113"

